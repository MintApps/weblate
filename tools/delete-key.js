/**
 * Delete a specific key from all languages, specified on the console in the form XXX.yyy.
 * Important: Before using this tool, the weblate repository must be committed, pushed and locked in codeberg.
 * @author Thomas Kippenberg
 * @module delete-key
 */

import config from './config.js'
import fs from 'fs'

/**
 * main function
 */
async function main() {
  // read and check param
  const param = process.argv[2]
  if (process.argv[3] || !param || param.split('.').length !== 2) {
    console.log('Key is missing or wrong format')
    process.exit()
  }
  const ns =  param.split('.')[0]
  const key = param.split('.')[1]

  if (!ns || !key) {
    console.log('Namespace or key invalid')
  }

  for (const locale of config.locales) {
    const messages = (await import(`../main-${locale}.json`, { with: { type: "json" } })).default
    if (messages[ns] && messages[ns][key]) {
      delete messages[ns][key]
      fs.writeFileSync( `./main-${locale}.json`, JSON.stringify(messages, null, 4) )
      console.log(`${locale}: deleted`)
    }
  }
}

main()