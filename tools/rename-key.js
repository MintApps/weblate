/**
 * Rename a specific translation key in the form AAA.bbb to another in the same form CCC.ddd.
 * The renaming is performed for all languages.
 * Important: Before using this tool, the weblate repository must be committed, pushed and locked in codeberg.
 * @author Thomas Kippenberg
 * @module rename key
 */

import config from './config.js'
import fs from 'fs'

async function main () {
  // read and check param
  const param1 = process.argv[2]
  const param2 = process.argv[3]
  if (!param1 || param1.split('.').length !== 2 || !param2 || param2.split('.').length !== 2) {
    console.log('Key is missing or wrong format')
    process.exit()
  }
  const ns1 =  param1.split('.')[0]
  const key1 = param1.split('.')[1]
  const ns2 =  param2.split('.')[0]
  const key2 = param2.split('.')[1]

  if (!ns1 || !key1 || !ns2 || !key2) {
    console.log('Namespace or Key invalid')
  }

  for (const locale of config.locales) {
    const messages = (await import(`../main-${locale}.json`, { with: { type: "json" } })).default
    if (messages[ns1] && messages[ns1][key1]) {
      const text = messages[ns1][key1]
      if (!messages[ns2]) messages[ns2] = {}
      if (text) messages[ns2][key2] = text
      delete messages[ns1][key1]
      fs.writeFileSync( `./main-${locale}.json`, JSON.stringify(messages, null, 4) )
      console.log(`${locale}: renamed`)
    }
  }
}

main ()