/**
 * Search for duplicates within the strings; the search is only carried out for the base language German
 * @author Thomas Kippenberg
 * @module find-duplicates
 */

import localeDe from '../main-de.json' with { type: 'json' }
const cache = []

/**
 * main function
 */
function main () {
  for (const ns1 of Object.keys(localeDe)) {
    for (const key1 of Object.keys(localeDe[ns1])) {
      for (const ns2 of Object.keys(localeDe)) {
        const text1 = localeDe[ns1][key1]
        for (const key2 of Object.keys(localeDe[ns2])) {
          const text2 = localeDe[ns2][key2]
          if (text1 === text2 && (ns1 !== ns2 || key1 !== key2)) {
            const cashKey1 = `${ns1}.${key1}`
            const cashKey2 = `${ns2}.${key2}`
            if (cache.indexOf(cashKey1) < 0 && cache.indexOf(cashKey2)) {
              console.log(`${ns1}.${key1}, ${ns2}.${key2}, "${text1}"`)
              cache.push(cashKey1, cashKey2)
            }
          }
        }
      }
    }
  }
}

main()