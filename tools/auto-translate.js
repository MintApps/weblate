/**
 * Translate all missing strings (for all languages) using a local instance of libreTranslate.
 * Important: Before using this tool, the weblate repository must be committed, pushed and locked in codeberg.
 * @author Thomas Kippenberg
 * @see {@link https://github.com/LibreTranslate/LibreTranslate|LibreTranslate}
 * @module auto-translate
 */

import config from './config.js'
import fs from 'fs'
import messagesDe from '../main-de.json' with { type: 'json' }
import messagesEn from '../main-en.json' with { type: 'json' }
import axios from 'axios'

const regexVariables = /{[a-zA-Z]+}/g
const regexMath = /``[0-9a-zA-Z _^().,;+-]+``+/g
// languages either translated by native speakers or not yet supported by MintApps
const noAutoTranslate = ['de', 'en', 'uk', 'id', 'zu', 'ru']

/**
 * Main function
 */
async function main() {
  // loop through all foreign languages configured for libre translate
  for (const locale of config.locales) {
    if (noAutoTranslate.indexOf(locale) < 0) {
      const messages = (await import(`../main-${locale}.json`, { with: { type: 'json' } })).default
      // loop all keys in German language
      for (const ns of Object.keys(messagesDe)) {
        if (!messages[ns]) messages[ns] = {}
        if (!messagesEn[ns]) messagesEn[ns] = {}
        for (const key of Object.keys(messagesDe[ns])) {
          // translation missing?
          if (!messages[ns][key] || messages[ns][key].trim().length === 0) {
            if (messagesEn[ns][key]) {
              // save all variables and math expressions
              const msg = messagesEn[ns][key]
              const variables = msg.match(regexVariables)
              const maths =  msg.match(regexMath)
              const trans = await axios.post(`${config.libreTranslate.url}/translate`, {
                q: msg,
                source: 'en',
                target: locale.substring(0, 2),
                api_key: config.libreTranslate.apiKey
              })
              let text = trans.data.translatedText
              if (text) {
                // reinsert variables and math
                if (variables) text = text.replaceAll(regexVariables, () => variables.shift())
                if (maths) text = text.replaceAll(regexMath, () => maths.shift())
                messages[ns][key] = text
                console.log(`en → ${locale}\n${messagesEn[ns][key]}\n${text}\n---`)
              } else {
                console.log(`Error when translating ${ns}/${key} into ${locale}`)
              }
            } else {
              console.log(`English translation for ${ns}/${key} is missing`)
            }
          }
        }
      }
      // save translations
      fs.writeFileSync( `./main-${locale}.json`, JSON.stringify(messages, null, 4) )
    }
  }
}

main()