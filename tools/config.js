/**
 * Configuration file for tools, specifying supported languages and local directories
 * @author Thomas Kippenberg
 * @module config
 */
export default {
  // supported locales (should match configured locales in weblate)
  locales: ['de', 'da', 'en', 'es', 'et', 'fi', 'fr', 'id', 'it', 'nl', 'pl', 'pt', 'ru', 'sv', 'es', 'cs', 'uk', 'hu', 'zu', 'zh_Hans'],
  // path to client source
  clientSrc: '../client/app',
  // path to weblate directory
  weblateDir: './',
  // LibreTranslate configuration
  libreTranslate: {
    url: 'http://127.0.0.1:5000',
    apiKey: ''
  }
}