/**
 * Search for URLs in all texts (app and info texts) and check these URLs by automatically calling them.
 * Important: Before using this tool, the weblate repository must be committed, pushed and locked in codeberg.
 * @author Thomas Kippenberg
 * @module find-urls
 */

import mainDe from '../main-de.json' with { type: 'json' }
import infosDe from '../infos-de.json' with { type: 'json' }
import axios from 'axios'

/** regex used to identifiy urls */
const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
/** user agent used for requests */
const userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246'

/**
 * main function
 */
async function main() {
  const allUrls = []
  // app locales
  Object.keys(mainDe).forEach(ns => {
    Object.keys(mainDe[ns]).forEach(key => {
      const text = mainDe[ns][key]
      const urls = text.match(urlRegex)
      if (urls) {
        allUrls.push(...urls)
        /*for (const url of urls) {
          console.log(`${url}\t${ns}.${key}`)
        }*/
      }
    })
  })
  // info locales
  Object.keys(infosDe).forEach(ns => {
    const text = infosDe[ns]
    if (typeof text === 'string') {
      const urls = text.match(urlRegex)
      if (urls) {
        allUrls.push(...urls)
        /*for (const url of urls) {
          console.log(`${url}\t${ns}`)
        }*/
      }
    }
  })

  // sort output and remove duplicates
  var urlsUniqueAndSorted = [...new Set(allUrls)].sort() 
  console.log(`Number of unique links: ${urlsUniqueAndSorted.length}`)
  
  // check links
  console.log('\nChecking for broken links, this might take some time...')
  let count = 0
  for (const url of urlsUniqueAndSorted) {
    try {
      await axios.get(url, { 
        responseType: 'document',
        headers: { 'User-Agent': userAgent},
        signal: AbortSignal.timeout(5000)
      })
    } catch (err) {
      count++
      console.log(`${url}\t${err?.response?.status}\t${err?.response?.statusText}`)
    }
  }
  console.log(`\nNumber of broken links: ${count}`)
}

main()