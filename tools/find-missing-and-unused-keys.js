/**
 * Search for missing translations (i.e. for keys that appear in the program code but have not yet been translated),
 * as well as for translations that are no longer needed (i.e. whose key no longer appears in the program code).
 * The search is only carried out for the base language German.
 * @author Thomas Kippenberg
 * @module find-missing-and-unused-keys
 */


import config from './config.js'
import fs from 'fs'
import path from 'path'
import localeDe from '../main-de.json' with { type: 'json' }
const cache = {}
const fileList = []

function main() {
  // identify relvant files
  console.log('\n1) Searching for files\n')
  const dirs = fs.readdirSync(config.clientSrc)
  for (const dir of dirs) {
    if (fs.statSync(path.join(config.clientSrc, dir)).isDirectory()) {
      const files = fs.readdirSync(path.join(config.clientSrc, dir))
      for (const file of files) {
        if (file.endsWith('html') || file.endsWith('js')) {
          fileList.push(path.join(config.clientSrc, dir, file))
        }
      }
    }
  }
  fileList.push(path.join(config.clientSrc, 'sitemap.js'))
  console.log(`Found ${fileList.length} files`)

  /* read all html files in /src and look for missing keys */
  console.log('\n2) Missing keys\n')
  const i18nKeywords = [
    'translate(', 'data-i18-md-block', 'data-i18-md-inline', 'data-i18-md', 'data-text', 'data-title', 'data-subtitle'
  ]
  for (const file of fileList) {
    const text = fs.readFileSync(file, 'utf8')
    let pos = 0
    while (pos < text.length) {
      let posKeyword = -1
      let posStart = -1
      let delimitor
      // try all keywords
      for (let i = 0; posKeyword < 0 && i < i18nKeywords.length; i++) {
        posKeyword = text.indexOf(i18nKeywords[i], pos)
        if (posKeyword >= 0) {
          posStart = posKeyword + i18nKeywords[i].length + 1
          delimitor = text.charAt(posStart)
        }
      }
      if (posStart > 0 && (delimitor === '\'' || delimitor === '"')) {
        let posEnd = text.indexOf(delimitor, posStart + 1)
        if (posEnd > 0) {
          const nskey = text.substring(posStart + 1, posEnd)
          const ns = nskey.split('.')[0]
          const key = nskey.split('.')[1]
          if (ns && !cache[ns]) cache[ns] = {}
          if (ns && key) cache[ns][key] = true
          if (ns && key && (!localeDe[ns] || !localeDe[ns][key])) {
            console.log(nskey)
          }
          pos = posEnd + 2
        } else {
          pos = posStart
        }
      } else {
        pos = text.length
      }
    }
  }

  console.log('\n3) Unused keys\n')
  // read all files to memory...
  const textList = []
  for (const file of fileList) {
    textList.push(fs.readFileSync(file, 'utf8'))
  }
  // search keys
  for (const ns of Object.keys(localeDe)) {
    for (const key of Object.keys(localeDe[ns])) {
      const text = localeDe[ns][key]
      if (text && key !== 'menu' && (!cache[ns] || !cache[ns][key])) {
        // possibly missing key - now search for exact match in all files
        let missing = true
        for (const text of textList) {
          if (
            text.indexOf(`${ns}.${key}`) >= 0 ||
            ns === 'General' ||
            ns === 'Errors' ||
            ns === 'Substances' ||
            ns === 'Colors' ||
            ns === 'Maths' ||
            ns === 'Physics' ||
            (ns === 'Schroedinger' && key.endsWith('desc') >= 0) ||
            (ns === 'Vidana' && key.startsWith('info-step') >= 0) ||
            (ns === 'Games' && key.startsWith('info'))
          ) {
            missing = false
            break
          }
        }
        if (missing) console.log(`${ns}.${key}`)
      }
    }
  }
}

main()