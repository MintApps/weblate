# MintApps Language files

## General information

- The MintApps are a collection of HTML5 apps to support teaching in technical and scientific subjects.
- The apps are based on the Vue framework.
- As a rule, each component has its own namespace in Weblate, for example the key MintGameEditInfo.info1 belongs to the "MintGameEditInfo" component.
- In addition to the component-related namespaces, there are some more: General, Menus, Errors, Maths, Physics, and School for frequently used texts

## Important notes on translation

- The source language is German.
- Strings enclosed in curly brackets must not be translated. For example, the string »Das Flugzeug fliegt mit einer Geschwindigkeit von {anzahl} Knoten« becomes »The aircraft is traveling at a speed of {anzahl} knots«.
- Strings enclosed by  ' ' characters must not be changed, these are mathematical formulas in AsciMath syntax. For example, »Die Lösungsformel ist ''x(1,2)=(-b+-sqrt(b\^2-4\*a\*c))/(2\*a)''« becomes »The solution formula is ''x(1,2)=(-b+-sqrt(b\^2-4\*a\*c))/(2\*a)''«

## Adding new languages

- We are always happy when new languages are added to the project.
- However, please note that languages are only activated on the official test installation if they are more than 5% translated.

## Build documentation

The tools are commented using jsdoc, to generate the documentation web pages, run the following command:

~~~
npm run doc
~~~

## Tools

The Tools folder contains several small scripts that are used to maintain the texts. These are for internal use only...

- auto-translate: Generate machine translations for all missing strings
- delete-key: Delete single key in *all* languages
- find-duplicates: Find duplicate keys in *German*
- find-missing-and-unused-keys: Find missing and unused keys in *German*
- find-urls: Find and check all urls
- rename-key: Rename single key for *all* languages
